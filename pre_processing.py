
# coding: utf-8

# # Click Through Rate Prediction - Data Pre-processing
# For this project we decided to use Python/R with H2O. However due to the limitations in the H2OFrame API for data manipulation, we decided to use SFrames by [GraphLab Create](https://turi.com/products/create/) which has a more mature API for data manipulation. Since SFrame is an out-of-core data structure, we are able to process very large datasets as long as we don't run out of both memory and disk space.
# 
# ## Installation
# We used python pip to install SFrames.
# 1. [Install GraphLab](https://turi.com/download/install.html)
# 2. Install the ```graphlab-create``` Python package
# 3. Install the ```sframe``` python package 
# ```
# pip install -U sframe
# pip install -U graphlab-create
# pip install --upgrade --no-cache-dir https://get.graphlab.com/GraphLab-Create/2.1/your registered email address here/your product key here/GraphLab-Create-License.tar.gz
# ```
# For this project we want to use SFrames to join two files which have millions of rows. The first file is ```training.txt``` and the second file is ```userid_profile.txt```. The userid_profile.txt has gender and age information that we think might be useful when creating predictive models.

# ### Import graphlab 
# ```import graphlab as gl```

# In[1]:

import graphlab as gl


# ### Load data
# Load the ```training.txt``` data into an SFrame called data and ```userid_profile.txt``` into an SFrame called ```user_profile``` name the columns

# In[2]:

import graphlab as gl
data = gl.SFrame.read_csv('training.txt', delimiter='\t', header=False)
data.rename({'X1': 'clicks', 'X2':'impressions', 'X3':'display_url'
             , 'X4':'ad_id', 'X5':'advertiser_id', 'X6':'depth', 'X7':'position'
            , 'X8':'query_id', 'X9':'keyword_id', 'X10':'title_id', 'X11':'description_id'
             , 'X12':'user_id'})
user_profile = gl.SFrame.read_csv('userid_profile.txt', delimiter='\t', header=False)
user_profile.rename({'X1': 'user_id', 'X2':'gender', 'X3':'age'})


# ### Validation
# View the top 5 rows for each of the files loaded above to ensure that the data loaded properly

# In[3]:

print 'Row count for data is {} \n'.format(data.num_rows())
print data.head(5)
print '\n'
print user_profile.head(5)
print '\n'
print 'Row count for user_profile is {}\n'.format(user_profile.num_rows())


# ### Decode gender
# Apply the following transformations to the gender column in the user_profile SFrame
# 1. if gender is 1 replace with M
# 2. if gender is 2 replace with F
# 3. if gender is 0 replace with UNKNOWN

# In[4]:

def decode_gender(gender):
    if gender == 1:
        return 'M'
    elif gender == 2:
        return 'F'
    else:
        return 'UNKNOWN'
    
user_profile['gender'] = user_profile['gender'].apply(lambda x: decode_gender(x))
# sample results of the transformation
user_profile.head(5)


# ### Join
# join data and user_profile on user_id into one SFrame and show the column names

# In[5]:

data = data.join(user_profile, on='user_id', how='left')
data.column_names()


# ### Save
# Save the SFrame to a csv file

# In[6]:

data.export_csv('pre_processed.csv', delimiter=',')


# ### Validation
# assert the number of rows in the saved file is equal to 149,639,105

# In[12]:

assert(data.num_rows()==149639105)

