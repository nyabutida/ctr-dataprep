ProjectPlan
Details
Activity
LAST WEEK

You edited an item
StackEdit • Sat 2:08 AM
Text
ProjectPlan

You moved an item to
Fri 1:10 AM
Google Drive Folder
SEIS 734
Text
ProjectPlan

You uploaded an item
StackEdit • Fri 1:08 AM
Text
ProjectPlan
No recorded activity before October 21, 2016
All selections cleared
New Team Drive

# SEIS 734 - Data Mining - Project Plan

>**Group Members** 
>Michael Xiong
Geoffrey Lamb
Tong Wu
Chennu Fan
Davis Nyabuti
Sheela Ponnusamy

##**Click Through Rate Prediction**

###October 15, 2016 

###**Problem Statement:** 

Click Through Rate (CTR) is a metric used to measure the effectiveness of a marketing campaign to bring people to a website. CTR is the ratio of clicks and impressions of a digital advertisement. Advertisements (Ads) can be served through search engines or email campaigns. Ads with a higher CTR are more effective than those with lower CTR. Therefore it is very important to focus on Ads with a higher CTR in order to have a successful marketing campaign. The marketing campaigns and the landing pages for each Ad are designed to elicit some form of action from the user. The actions could include buying a product or signing up for a service/newsletter. 

Different users react differently to Ads. Therefore, it is important to predict the CTR at a user level. User level predictions however, might be too granular to generate good CTR prediction models. It is probably better to group users based on their click data using clustering algorithms. Using classification we can Identify target classes for each Ad. This will allow us to rank Ads based on how effective they are at generating clicks.

###**References** 

**A Two-Stage Ensemble of Diverse Models for Advertisement Ranking in KDD Cup 2012. (2012)**
**Retrieved from** [http://www.csie.ntu.edu.tw/~htlin/paper/doc/wskdd12cup.pdf](http://www.csie.ntu.edu.tw/~htlin/paper/doc/wskdd12cup.pdf)

In this paper, researchers employed two general approaches for predicting CTR: naïve Bayes and logistic regression. Before constructing models, the researchers first split the data into positive samples (#click) and negative samples (#impression - #click) in order to determine how well each model could discriminate between positive and negative samples. In the naïve Bayes approach, a predictive model was developed by using estimated conditional probability as a ranking criteria for the attributes in the dataset. The result of this process determined that Ad ID, position, UserID, and QueryID were useful attributes for predicting CTR. On the other hand, the researchers tried out several different logistic regression models to predict CTR directly using the attributes in the dataset. In terms of AUC, the regression models performed slightly better than naïve Bayes at classifying CTR. 
_____

**COMPACT WEB BROWSING PROFILES FOR CLICK-THROUGH RATE PREDICTION (2014)**
**Retrieved from** [http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6958852](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=6958852) 
This paper compares three models for click-through rate: the Singular Value Decomposition (SVD), Infinite Relational Model (IRM), and Non-negative Matrix Factorization (NMF). The data sets used in the paper originate from Ad form’s ad transaction logs. They first pick up a period of time, join the log, and finally strip query-string in the URL. After applying all three methods on the same data set, they got a conclusion that the IRM is the best (high-speed and high throughput). 
__________
**Predicting Clicks: CTR Estimation of Advertisements using Logistic Regression Classifier (2015)**
**Retrieved from** [http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7154880](http://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=7154880) 
In this paper, they applied prediction algorithm on 7 days advertisement dataset of size 25 GB collected from a Knowledge Discovery and Data Mining (KDD) cup website. They use feature extraction to pre-processing the data and logistic regression classifier to estimate the CTR. The conclusion is that they are able to achieve around 90% accuracy for CTR estimation by using this classification model. 
_____________________
**Web-Scale Bayesian Click-Through Rate Prediction for Sponsored Search Advertising in Microsoft’s Bing Search Engine (2010)**
**Retrieved from** [https://www.microsoft.com/en-us/research/publication/web-scale-bayesian-click-through-rate-prediction-for-sponsored-search-advertising-in-microsofts-bing-search-engine/](https://www.microsoft.com/en-us/research/publication/web-scale-bayesian-click-through-rate-prediction-for-sponsored-search-advertising-in-microsofts-bing-search-engine/)
This paper is about the Sponsored search engine which remains one of the most profitable business models on the web to make majority of income through the three major search engines Google, Yahoo and Bing. All three major players use keyword auctions to allocate display space alongside the algorithmic search results based on a pay-per-click model in which advertisers are charged only if their advertisements are clicked by a user. The keyword auction works as for a given product or service advertisers identify suitable keywords likely to be typed by users interested in their offering. The search engine to estimate the click-through rate (CTR) of available ads for a given search query. When any ad is clicked the search engine get the keyword and search which advertisers are eligible to participate in an auction. Then it allocates the available ad positions to the ads in the auction and needs to determine appropriate payments. Bayesian click-through rate (CTR) prediction algorithm used for Sponsored Search in Microsoft’s Bing search engine. The algorithm is based on a probit regression model that maps discrete or real-valued input features to probabilities.

It describes the Sponsored Search application scenario, the key role of CTR prediction in general, and the particular constraints derived from the task, including accuracy, calibration, scalability, dynamics, and exploration. Also it describes a new Bayesian online learning algorithm for binary prediction, subsequently referred to as adPredictor. As in most data mining, problems in constructing and selecting good features is one of the core challenges. For the learning algorithm one of the resulting challenges is the requirement to be able to handle discrete features of very different cardinalities, e.g., a two-valued feature such as gender and a billion-valued feature such as user ID.

### **Goals:** 

1. **Predict CTR:** Based on the user profile we can determine what Ads to serve in order to reduce marketing campaign costs. When the Ads are targeted to the right audience the CTR increases.

### **Data Selection** 

The dataset used in this project was from 2012 KDD Cup challenge http://www.kddcup2012.org/c/kddcup2012-track2

### **E-R Diagram** 

![CTR Data ER Diagram](https://www.lucidchart.com/publicSegments/view/4e869a4e-9182-431e-ac5f-d0fb08afe31c/image.png)


#### <i class="icon-file"></i>***training.txt schema*** 

1. **clicks:** The number of clicks on the Ad
2. **impressions:** The number of times an Ad was viewed by a user
3. **display_url: ** A property of the Ad
3. **ad_id:**  A unique identifier for an Ad
4. **advertiser_id:** A unique identifier for an advertiser
5. **depth:** A property of the user session
6. **position:** The positioning of an Ad on a webpage. This is a property of a session
7. **query_id:** An Id of the query that was submitted before the Ad was displayed
8. **keyword_id:** A unique identifier for a keyword
9. **title_id:** a property of the Ad
10. **description_id:** a property of the Ad
11. **user_id:** A unique identifier for a user

#### <i class="icon-file"></i>***userid_profile.txt schema*** 

1. **user_id:**
2. **gender** '1'  for male, '2' for female,  and '0'  for unknown. 
1. **age:** the age group for a user. '1'  for (0, 12],  '2' for (12, 18], '3' for (18, 24], '4'  for  (24, 30], '5' for (30,  40], and '6' for greater than 40

### <i class="icon-cog"></i>Data Pre-Processing 

For this project we decided to use Python/R with H2O. However due to the limitations in the H2OFrame API for data manipulation, we decided to use SFrames by [GraphLab Create](https://turi.com/products/create/) which has a more mature API for data manipulation. Since SFrame is an out-of-core data structure, we are able to process very large datasets as long as we don't run out of both memory and disk space.

## Installation 

We used python pip to install SFrames.
1. [Install GraphLab](https://turi.com/download/install.html)
2. Install the ```graphlab-create``` Python package
3. Install the ```sframe``` python package 
```
pip install -U sframe
pip install -U graphlab-create
pip install --upgrade --no-cache-dir https://get.graphlab.com/GraphLab-Create/2.1/your registered email address here/your product key here/GraphLab-Create-License.tar.gz
```
For this project we want to use SFrames to join two files which have millions of rows. The first file is ``training.txt`` and the second file is ``userid_profile.txt``. The userid_profile.txt has gender and age information that we think might be useful when creating predictive models.

###  Import graphlab  

```python
import graphlab as gl
```

### <i class="icon-upload"></i>Load data 

Load the ```training.txt``` data into an SFrame called ```data``` and ```userid_profile.txt``` into an SFrame called ```user_profile``` name the columns


```python
import graphlab as gl
data = gl.SFrame.read_csv('training.txt', delimiter='\t', header=False)
data.rename({'X1': 'clicks', 'X2':'impressions', 'X3':'display_url'
             , 'X4':'ad_id', 'X5':'advertiser_id', 'X6':'depth', 'X7':'position'
            , 'X8':'query_id', 'X9':'keyword_id', 'X10':'title_id', 'X11':'description_id'
             , 'X12':'user_id'})
user_profile = gl.SFrame.read_csv('userid_profile.txt', delimiter='\t', header=False)
user_profile.rename({'X1': 'user_id', 'X2':'gender', 'X3':'age'})
```

    This non-commercial license of GraphLab Create for academic use is assigned to nyab3075@stthomas.edu and will expire on October 20, 2017.


    [INFO] graphlab.cython.cy_server: GraphLab Create v2.1 started. Logging: /tmp/graphlab_server_1477017001.log



<pre>Finished parsing file /home/davis/seis735/training.txt</pre>



<pre>Parsing completed. Parsed 100 lines in 1.2141 secs.</pre>


    ------------------------------------------------------
    Inferred types from first 100 line(s) of file as 
    column_type_hints=[int,int,str,int,int,int,int,int,int,int,int,int]
    If parsing fails due to incorrect types, you can correct
    the inferred type list above and pass it to read_csv in
    the column_type_hints argument
    ------------------------------------------------------



<pre>Read 738350 lines. Lines per second: 408640</pre>



<pre>Read 5149800 lines. Lines per second: 690998</pre>



<pre>Read 9565384 lines. Lines per second: 748238</pre>



<pre>...</pre>



<pre>Read 142177371 lines. Lines per second: 802635</pre>



<pre>Read 146594577 lines. Lines per second: 801874</pre>



<pre>Finished parsing file /home/davis/seis735/training.txt</pre>



<pre>Parsing completed. Parsed 149639105 lines in 186.274 secs.</pre>



<pre>Finished parsing file /home/davis/seis735/userid_profile.txt</pre>



<pre>Parsing completed. Parsed 100 lines in 1.51216 secs.</pre>


    ------------------------------------------------------
    Inferred types from first 100 line(s) of file as 
    column_type_hints=[int,int,int]
    If parsing fails due to incorrect types, you can correct
    the inferred type list above and pass it to read_csv in
    the column_type_hints argument
    ------------------------------------------------------



<pre>Read 4461011 lines. Lines per second: 1.98733e+06</pre>



<pre>Read 16978904 lines. Lines per second: 2.30699e+06</pre>



<pre>Finished parsing file /home/davis/seis735/userid_profile.txt</pre>



<pre>Parsing completed. Parsed 23669283 lines in 8.19258 secs.</pre>





<div style="max-height:1000px;max-width:1500px;overflow:auto;"><table frame="box" rules="cols">
    <tr>
        <th style="padding-left: 1em; padding-right: 1em; text-align: center">user_id</th>
        <th style="padding-left: 1em; padding-right: 1em; text-align: center">gender</th>
        <th style="padding-left: 1em; padding-right: 1em; text-align: center">age</th>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">5</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">5</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">4</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">5</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">6</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">7</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">8</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">4</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">9</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">10</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">5</td>
    </tr>
</table>
[23669283 rows x 3 columns]<br/>Note: Only the head of the SFrame is printed.<br/>You can use print_rows(num_rows=m, num_columns=n) to print more rows and columns.
</div>



### <i class="icon-ok"></i>Validation 

Ensure the data loaded properly


```python
print 'Row count for data is {} \n'.format(data.num_rows())
print user_profile.head(5)
print '\n'
print 'Row count for user_profile is {}\n'.format(user_profile.num_rows())

``` 


```Row count for data is 149639105 ``` 

      
    +---------+--------+-----+
    | user_id | gender | age |
    +---------+--------+-----+
    |    1    |   1    |  5  |
    |    2    |   2    |  3  |
    |    3    |   1    |  5  |
    |    4    |   1    |  3  |
    |    5    |   2    |  1  |
    +---------+--------+-----+
    [5 rows x 3 columns]
    Row count for user_profile is 23669283
    


### Decode gender 

Apply the following transformations to the gender column in the user_profile SFrame
1. if gender is **1** replace with **M**
2. if gender is **2** replace with **F**
3. if gender is **0** replace with **UNKNOWN**


```python
def decode_gender(gender):
    if gender == 1:
        return 'M'
    elif gender == 2:
        return 'F'
    else:
        return 'UNKNOWN'
    
user_profile['gender'] = user_profile['gender'].apply(lambda x: decode_gender(x))
# sample results of the transformation
user_profile.head(5)
```

<div style="max-height:1000px;max-width:1500px;overflow:auto;"><table frame="box" rules="cols">
    <tr>
        <th style="padding-left: 1em; padding-right: 1em; text-align: center">user_id</th>
        <th style="padding-left: 1em; padding-right: 1em; text-align: center">gender</th>
        <th style="padding-left: 1em; padding-right: 1em; text-align: center">age</th>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">M</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">5</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">2</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">F</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">M</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">5</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">4</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">M</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">3</td>
    </tr>
    <tr>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">5</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">F</td>
        <td style="padding-left: 1em; padding-right: 1em; text-align: center; vertical-align: top">1</td>
    </tr>
</table>
[5 rows x 3 columns]<br/>
</div>



### Join

join data and user_profile on user_id into one SFrame and show the column names


```python
data = data.join(user_profile, on='user_id', how='left')
data.column_names()
```




    ['clicks',
     'impressions',
     'display_url',
     'ad_id',
     'advertiser_id',
     'depth',
     'position',
     'query_id',
     'keyword_id',
     'title_id',
     'description_id',
     'user_id',
     'gender',
     'age']



### <i class="icon-file"></i>Save

Save the SFrame to a csv file


```python
data.export_csv('pre_processed.csv', delimiter=',')
```

### <i class="icon-ok"></i>Validation

assert the number of rows in the saved file is equal to 149,639,105


```python
assert(data.num_rows()==149639105)
```

### **Datasets**

***dataset.zip*** file contains the following files
1. **pre_processing.ipynb** - ipython notebook of the preprocessing application
2. **pre_processing.py** - python script for pre-processing the data. When run it generates ```pre_processed.csv``` which a join of ```training.txt``` and ```userid_profile.txt```. The script does not need any parameters to run
3. **pre_processed.csv** - the output of our preprocessing program
4. **descriptionid_tokensid.txt**
5. **purchasedkeywordid_tokensid.txt**
6. **queryid_tokensid.txt**
7. **titleid_tokensid.txt**
8. **training.txt** - main data file
9. **userid_profile.txt** - contains user_id, gender and age